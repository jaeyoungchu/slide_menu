import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:slide_menu/screens/mainScreen.dart';
import 'package:slide_menu/screens/splashScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp
    ]);

    return MaterialApp(
      title: 'Slide Menu',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      routes: <String,WidgetBuilder>{
        '/mainScreen' : (BuildContext context) => MainScreen(),
      },
      home: SplashScreen(),
    );
  }
}

