import 'package:flutter/material.dart';
import 'package:decimal/decimal.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {

  AnimationController controller;
  double headerHeight = 32.0;
  Product currentProduct;
  List<Product> allProducts;
  int cartQuantity;
  bool showCompleteOrder,isConfirmed;
  double screenHeight;
  double screenWidth;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    currentProduct = null;
    allProducts = getAllProductList();
    cartQuantity = 0;
    showCompleteOrder = false;
    isConfirmed = false;
    controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 400), value: 1.0);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Slide Menu',
            style: TextStyle(
                color: Colors.white,
                fontFamily: 'Charm',
                fontSize: 26.0,
                fontWeight: FontWeight.bold),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.menu,
              color: Colors.white,
            ),
            onPressed: () {
              getStyledDialog(context);
            },
          ),
          actions: <Widget>[
            Stack(
              children: <Widget>[
                IgnorePointer(
                  ignoring: isConfirmed,
                  child: IconButton(
                    icon: Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ),
                    onPressed: (){onCartPress();},
                  ),
                ),
                cartQuantity == 0
                    ? Container()
                    : Positioned(
                        left: 2.0,
                        top: 2.0,
                        child: Container(
                          width: 20.0,
                          height: 20.0,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Center(
                              child: Text(
                            "$cartQuantity",
                            style: TextStyle(color: Colors.white),
                          )),
                        ),
                      ),
              ],
            ),
          ],
        ),
        body: LayoutBuilder(
          builder: panels,
        ),
      ),
    );
  }

  Widget panels(BuildContext context, BoxConstraints constraints) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            child: getListView(),
          ),
          PositionedTransition(
            rect: getPanelAnimation(constraints),
            child: Material(
              elevation: 12.0,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16.0),
                topRight: Radius.circular(16.0),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    height: headerHeight,
                    child: Center(
                      child: IgnorePointer(
                        ignoring: (cartQuantity == 0 && isPanelVisible)
                            ? true
                            : false,
                        child: ConstrainedBox(
                          constraints:
                              BoxConstraints(minWidth: double.infinity),
                          child: IgnorePointer(
                            ignoring: isConfirmed,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(16.0),
                                topRight: Radius.circular(16.0),
                              )),
                              elevation: 1.0,
                              onPressed: () {
                                flingPanel(null,true);
                              },
                              color: Colors.red,
                              highlightColor: Colors.orange[100],
                              child: Text(
                                getButtonText(),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  getFrontPanel(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  String getButtonText() {
    if (isPanelVisible && cartQuantity == 0) {
      return "";
    } else if (isPanelVisible) {
      return "Complete Order";
    } else {
      return "Back";
    }
  }

  void flingPanel(Product product,bool showCompleteOrder) {

    if(!isPanelVisible){
      controller.fling(velocity: isPanelVisible ? -1.0 : 1.0);
      setState(() {
        this.showCompleteOrder = false;
      });
      return;
    }

    if (product == null && showCompleteOrder == false) {
      controller.fling(velocity: isPanelVisible ? -1.0 : 1.0);
      return;
    }
    if(showCompleteOrder){
      setState(() {
        this.showCompleteOrder = true;
      });
      controller.fling(velocity: isPanelVisible ? -1.0 : 1.0);
      return;
    }
    setState(() {
      this.currentProduct = product;
    });
    controller.fling(velocity: isPanelVisible ? -1.0 : 1.0);
  }

  void onCartPress() {
    if(!isPanelVisible || getCartQuantity() ==0){
      return;
    }
    flingPanel(null, true);
  }

  Widget getFrontPanel() {
    if (currentProduct == null) {
      return Container(
        child: Center(
          child: Text("Invalid product."),
        ),
      );

    }
    if(showCompleteOrder){
      return getOrderList();
    }
    return Expanded(
      child: Container(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 10.0,
            ),
            Expanded(
              flex: 4,
              child: Container(
                  width: double.infinity,
                  child: Image.network(
                    currentProduct.imagePath,
                    fit: BoxFit.fitWidth,
                  )),
            ),
            SizedBox(
              height: 10.0,
            ),
            Expanded(
              flex: 4,
              child: Column(
                children: <Widget>[
                  Expanded(
                      flex: 3,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex : 8,
                              child:
                                  getStyledTextBox(currentProduct.productName)),
                          Expanded(
                              flex : 2,
                              child:
                              getStyledTextBox('${currentProduct.quantity}')),
                        ],
                      )),
                  Expanded(
                    flex: 2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                            flex: 1,
                            child:
                                getStyledTextBox("\$${currentProduct.price}")),
                        Expanded(
                            flex: 1,
                            child: getStyledTextBox(
                                "\$${Decimal.parse("${currentProduct.price}") * Decimal.parse("${currentProduct.quantity}")}")),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: RaisedButton(
                                  onPressed: () {
                                    setQuantity(true);
                                  },
                                elevation: 4.0,
                                color: Colors.red,
                                highlightColor: Colors.orange[400],
                                child: Text("+", style: TextStyle(color: Colors.white,fontSize: 40.0),),
                              ),
                            ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: RaisedButton(
                              onPressed: () {
                                setQuantity(false);
                              },
                              elevation: 4.0,
                              color: Colors.red,
                              highlightColor: Colors.orange[400],
                              child: Text("-", style: TextStyle(color: Colors.white,fontSize: 40.0),),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget getStyledTextBox(String text) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.orange, width: 1.0),
        ),
        child: Center(
          child: Text(
            text,
            style: TextStyle(color: Colors.red, fontSize: 22.0),
          ),
        ),
      ),
    );
  }

  Animation<RelativeRect> getPanelAnimation(BoxConstraints constraints) {
    final height = constraints.biggest.height;
    final openedHeight = height - headerHeight;
    final closedHeight = -headerHeight;

    return RelativeRectTween(
            begin: RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0),
            end: RelativeRect.fromLTRB(0.0, openedHeight, 0.0, closedHeight))
        .animate(CurvedAnimation(parent: controller, curve: Curves.linear));
  }

  Widget getListView() {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    final categoryList = getDummyCategoryNames();

    if (categoryList.categoryNames.length == 0) {
      return Scaffold(
        body: Center(
            child: CircularProgressIndicator(
          backgroundColor: Colors.orange,
          strokeWidth: 5.0,
        )),
      );
    }
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          final categoryName = categoryList.categoryNames[index];
          return Padding(
            padding: index == categoryList.categoryNames.length - 1
                ? EdgeInsets.fromLTRB(3.0, 3.0, 3.0, 40.0)
                : EdgeInsets.all(3.0),
            child: Card(
              elevation: 3.0,
              child: Container(
                height: screenHeight / 2,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            stops: [0.2, 0.7, 0.8, 0.9],
                            colors: [
                              Colors.orange[800],
                              Colors.orange[700],
                              Colors.orange[600],
                              Colors.orange[500],
                            ],
                          ),
                        ),
                        child: Center(
                          child: Text(
                            categoryList.categoryNames[index],
                            style: TextStyle(
                                fontSize: 22.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontFamily: 'Indie'),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 8,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: getCategoryProducts(categoryName).length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () {
                                flingPanel(
                                    getCategoryProducts(categoryName)[index],false);
                              },
                              child: AspectRatio(
                                aspectRatio: 100 / 120,
                                child: Padding(
                                  padding: EdgeInsets.all(4.0),
                                  child: Card(
                                    elevation: 1.0,
                                    child: Column(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 7,
                                          child: Container(
                                            width: screenWidth / 3 * 2,
//                                        height: screenHeight/3,
                                            child: Image.network(
                                              getCategoryProducts(
                                                      categoryName)[index]
                                                  .imagePath,
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                begin: Alignment.topLeft,
                                                end: Alignment.bottomRight,
                                                stops: [0.2, 0.7, 0.8, 0.9],
                                                colors: [
                                                  Colors.orange[800],
                                                  Colors.orange[700],
                                                  Colors.orange[600],
                                                  Colors.orange[500],
                                                ],
                                              ),
                                            ),
                                            width: screenWidth / 3 * 2,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 5.0),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceAround,
                                                children: <Widget>[
                                                  Text(
                                                    getCategoryProducts(
                                                            categoryName)[index]
                                                        .productName,
                                                    softWrap: true,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 2,
                                                    style: TextStyle(
                                                        fontSize: 18.0,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white,
                                                        fontFamily: 'Indie'),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 10.0),
                                                    child: Text(
                                                      "\$${getCategoryProducts(categoryName)[index].price}",
                                                      style: TextStyle(
                                                          fontSize: 16.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.white,
                                                          fontFamily: 'Charm'),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }),
                    )
                  ],
                ),
              ),
            ),
          );
        },
        itemCount: categoryList.categoryNames.length,
      ),
    );
  }

  Widget getOrderList() {
    List<Product> orderList = getOrderedProducts();

    return Expanded(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Container(),
          ),
          Expanded(
            flex: 76,
            child: Container(
              child: ListView.builder(
                  itemCount: orderList.length,
                  itemBuilder: (BuildContext context,int index){
                    return GestureDetector(
                      onTap: (){_onTapToDel(orderList[index]);},
                      child: Card(
                        elevation: 4.0,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.network(orderList[index].imagePath),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Text('${orderList[index].productName}',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0,color: Colors.black87,fontFamily: 'Charm'),),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('${orderList[index].quantity}',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0,color: Colors.black87,fontFamily: 'Charm'),),
                            ),
                            Expanded(
                              flex: 2,
                              child: Align(
                                  alignment: Alignment(0.0, 1.0),
                                  child: Text("\$${Decimal.parse("${orderList[index].price}") * Decimal.parse("${orderList[index].quantity}")}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0,color: Colors.black87,fontFamily: 'Charm'),)),
                            )
                          ],
                        ),
                      ),
                    );
                  }
              ),
            ),
          ),
          Expanded(
            flex: 10,
            child: Card(
              child: Align(
                  alignment: Alignment(0.8, 0.0),
                  child: Text("Total Price  \$${getTotalPrice(orderList).toStringAsFixed(2)}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0,color: Colors.black87,fontFamily: 'Charm'),)),
            ),
          ),
          Expanded(
            flex: 10,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: double.infinity,
                child: RaisedButton(
                  color: isConfirmed ? Colors.orange : Colors.green,
                    onPressed: (){
                    setState(() {
                      isConfirmed = !isConfirmed;
                    });
                    },
                        child: isConfirmed ? Row(
                          children: <Widget>[
                            Expanded(
                              flex: 3,
                              child: Icon(Icons.check_circle,color: Colors.white,),
                            ),
                            Expanded(
                              flex: 6,
                              child: Text("Confirm",style: TextStyle(fontSize: 28.0,color: Colors.white,fontFamily: 'Indie',fontWeight: FontWeight.bold),),
                            )

                          ],
                        )
                            :
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 3,
                              child: Icon(Icons.check_circle,color: Colors.green,),
                            ),
                            Expanded(
                              flex: 6,
                              child: Text("Confirm",style: TextStyle(fontSize: 28.0,color: Colors.white,fontFamily: 'Indie',fontWeight: FontWeight.bold),),
                            )

                          ],
                        )

                ),
              ),
            ),
          )
        ],

      ),
    );
  }

  double getTotalPrice(List<Product> products){
    double total=0.0;

    for(var product in products){
      total += product.price *product.quantity;
    }

    return total;
  }

  List<Product> getCategoryProducts(String categoryName) {
    List<Product> products = new List();

    for (var product in allProducts) {
      if (product.categoryName == categoryName) {
        products.add(product);
      }
    }
    return products;
  }

  void setQuantity(bool isIncrease) {
    for (var product in allProducts) {
      if (product.productName == currentProduct.productName) {
        setState(() {
          if (isIncrease) {
            product.quantity++;
          } else {
            if (product.quantity == 0) {
              return;
            }
            product.quantity--;
          }
          setState(() {
            cartQuantity = getCartQuantity();
          });
        });
        break;
      }
    }
  }

  List<Product> getOrderedProducts(){

    List<Product> orderedProducts = new List();

    for (var product in allProducts) {
      if(product.quantity >0){
        orderedProducts.add(product);
      }
    }

    return orderedProducts;
  }

  int getCartQuantity() {
    int quantity = 0;

    for (var product in allProducts) {
      quantity += product.quantity;
    }

    if (quantity > 99) {
      quantity = 99;
    }

    return quantity;
  }

  bool get isPanelVisible {
    final AnimationStatus status = controller.status;

    return status == AnimationStatus.completed ||
        status == AnimationStatus.forward;
  }

  Future<bool> _onBackPressed() {
    if(isConfirmed){
      return null;
    }
    if (!isPanelVisible) {
      flingPanel(null,false);
    }
    return null;
//    return showDialog(
//        context: context,
//      builder:  (context) =>AlertDialog(
//        title: Text("Do you want to exit the app?"),
//        actions: <Widget>[
//          FlatButton(
//            child: Text("No",style: TextStyle(color: Colors.black,fontSize: 18.0),),
//            onPressed: ()=> Navigator.pop(context,false),
//          ),
//          FlatButton(
//            child: Text("Yes",style: TextStyle(color: Colors.black,fontSize: 18.0),),
//            onPressed: ()=> Navigator.pop(context,true),
//          )
//        ],
//      ),
//    );
  }

  getStyledDialog(context) {

    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return Dialog(
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Container(
              height: screenHeight/3,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    height: screenHeight/13,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10.0),
                            topLeft: Radius.circular(10.0)),
                        color: Colors.deepOrange),
                    child: Center(
                        child: Text(
                          "Owner Mode",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 24.0,
                          ),
                        )),
                  ),
                  Container(
                    height: screenHeight/8,
                    child: Center(
                      child: Material(
                        elevation: 4.0,
                        borderRadius: BorderRadius.circular(5.0),
                        child: TextFormField(
                            obscureText: true,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 20.0),
                                border: InputBorder.none,
                                prefixIcon: Icon(Icons.perm_identity,
                                    color: Colors.orange,
                                    size: 30.0),
                                hintText: "Pin Number",
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ))),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight/40,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: screenHeight/10,
                        child: Card(
                          elevation: 4.0,
                          child: FlatButton(
                            onPressed: (){_clearOrder();},
                            child: Text('Clear Order',style: TextStyle(color: Colors.deepOrange,fontSize: 18.0),),
                          ),
                        ),
                      ),
                      Container(
                        height: screenHeight/10,
                        child: Card(
                          elevation: 4.0,
                          child: FlatButton(
                            onPressed: null,
                            child: Text('Owner Page',style: TextStyle(color: Colors.deepOrange,fontSize: 18.0),),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }

  void _clearOrder() {
    if(true){
      Navigator.of(context).pop();
      _clearAllQuantities();
      if(!isPanelVisible){
        flingPanel(null, false);
      }
      setState(() {
        this.cartQuantity = getCartQuantity();
        this.isConfirmed = false;
      });
    }

  }

  void _clearAllQuantities(){
    for(var product in allProducts){
      product.quantity = 0;
    }
  }


  void _onTapToDel(Product product) {
    showDialog(
        context: context,
            builder: (BuildContext context){
              return new AlertDialog(
                content: Text('Do you want to delete ${product.productName}?'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Yes'),
                    onPressed: (){
                      _deleteOrder(product);
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('No'),
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            }
    );
  }

  void _deleteOrder(Product product) {
    for(Product p in allProducts){
      if(p.productName == product.productName){
        p.quantity = 0;
        setState(() {
          this.allProducts = allProducts;
          cartQuantity = getCartQuantity();
        });
      }
    }
  }
}

List<Product> getAllProductList() {
  List<Product> products = new List();

  products.add(new Product(
      "Coke",
      getDummyCategoryNames().categoryNames[0],
      'https://www.thebrobasket.com/wp-content/uploads/2016/06/Rum-and-Coke-141498-gal4.jpg',
      1.07));
  products.add(new Product(
      "Sprite",
      getDummyCategoryNames().categoryNames[0],
      'https://www.coca-cola.co.uk/content/dam/journey/gb/en/hidden/Products/lead-brand-image/sprite-lead-image-596x334-gb.jpg',
      1.07));
  products.add(new Product(
      "Orange Juice",
      getDummyCategoryNames().categoryNames[0],
      'https://img.etimg.com/thumb/msid-62564999,width-643,imgsize-275146,resizemode-4/yes-it-is-healthy-drinking-100-per-cent-fruit-juice-does-not-raise-blood-sugar-levels.jpg',
      1.87));
  products.add(new Product(
      "Double Burger",
      getDummyCategoryNames().categoryNames[1],
      'https://i1.wp.com/topsecretrecipes.com/foodhackerblog/wp-content/uploads/2018/03/In-n-out-double-double_top.jpg?fit=700%2C503&ssl=1',
      3.07));
  products.add(new Product(
      "Cheese Burger",
      getDummyCategoryNames().categoryNames[1],
      'https://www.thedailymeal.com/sites/default/files/2014/09/25/cheeseburger_1.jpg',
      2.07));
  products.add(new Product(
      "Angus Burger",
      getDummyCategoryNames().categoryNames[1],
      'https://i.pinimg.com/736x/10/4d/98/104d982199f7b048b6179a6cae4c65b4--angus-burger-mcdonalds.jpg',
      4.07));
  products.add(new Product(
      "Chicken Breast Salad",
      getDummyCategoryNames().categoryNames[2],
      'https://skinnyms.com/wp-content/uploads/2015/11/Skillet-Chicken-Herbs-with-Garden-Salad-1-750x499.jpg',
      3.57));
  products.add(new Product(
      "Arugula Salad",
      getDummyCategoryNames().categoryNames[2],
      'https://www.tasteslovely.com/wp-content/uploads/2014/04/Arugula-Salad-+-My-Favorite-Balsamic-Vinaigrette-tasteslovely.com_1.jpg',
      3.27));
  products.add(new Product(
      "Fajita Chicken Salad",
      getDummyCategoryNames().categoryNames[2],
      'http://assets.kraftfoods.com/recipe_images/opendeploy/106211_640x428.jpg',
      4.07));

  return products;
}

Categories getDummyCategoryNames() {
  Categories categories = new Categories();
  categories.categoryNames.add("Beverage");
  categories.categoryNames.add("Hamburger");
  categories.categoryNames.add("Salad");

  return categories;
}

class Categories {
  List<String> categoryNames = new List();
}

class Product {
  String productName, categoryName, imagePath;
  double price;
  int quantity = 0;

  Product(this.productName, this.categoryName, this.imagePath, this.price);
}
