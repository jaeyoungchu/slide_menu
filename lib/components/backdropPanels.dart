//import 'package:flutter/material.dart';
//import 'package:decimal/decimal.dart';
//
//class BackdropPanels extends StatefulWidget {
//  final AnimationController controller;
//
//  BackdropPanels({this.controller});
//
//  @override
//  _BackdropPanelsState createState() => _BackdropPanelsState();
//}
//
//class _BackdropPanelsState extends State<BackdropPanels> {
//  static const header_height = 32.0;
//
//  Product currentProduct;
//  List<Product> allProducts;
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    currentProduct = null;
//    allProducts = getAllProductList();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return LayoutBuilder(
//      builder: panels,
//    );
//  }
//
//  Widget panels(BuildContext context, BoxConstraints constraints) {
//    return Container(
//      child: Stack(
//        children: <Widget>[
//          Container(
//            child: getListView(),
//          ),
//          PositionedTransition(
//            rect: getPanelAnimation(constraints),
//            child: Material(
//              elevation: 12.0,
//              borderRadius: BorderRadius.only(
//                topLeft: Radius.circular(16.0),
//                topRight: Radius.circular(16.0),
//              ),
//              child: Column(
//                children: <Widget>[
//                  Container(
//                    height: header_height,
//                    child: Center(
//                      child: IgnorePointer(
//                        ignoring: false,
//                        child: ConstrainedBox(
//                          constraints:
//                              BoxConstraints(minWidth: double.infinity),
//                          child: RaisedButton(
//                            shape: RoundedRectangleBorder(
//                                borderRadius: BorderRadius.only(
//                              topLeft: Radius.circular(16.0),
//                              topRight: Radius.circular(16.0),
//                            )),
//                            elevation: 1.0,
//                            onPressed: () {
//                              flingPanel(null);
//                            },
//                            color: Colors.white,
//                            highlightColor: Colors.orange[100],
//                            child: Text(
//                              "Shop here",
//                              style: TextStyle(
//                                  color: Colors.black, fontSize: 20.0),
//                            ),
//                          ),
//                        ),
//                      ),
//                    ),
//                  ),
//                  getFrontPanel(),
//                ],
//              ),
//            ),
//          ),
//        ],
//      ),
//    );
//  }
//
//  void flingPanel(Product product) {
//    widget.controller.fling(velocity: isPanelVisible ? -1.0 : 1.0);
//    if (product == null) {
//      return;
//    }
//    setState(() {
//      this.currentProduct = product;
//    });
//
//  }
//
//  Widget getFrontPanel(){
//    if(currentProduct == null){
//      return Expanded(
//        child: Center(
//          child: Text("Invalid product",style: TextStyle(fontSize: 22.0),),
//        ),
//      );
//    }
//    return Expanded(
//      child: Container(
//        child: Column(
//          children: <Widget>[
//            SizedBox(
//              height: 10.0,
//            ),
//            Expanded(
//              flex: 4,
//              child: Container(
//                  width: double.infinity,
//                  child: Image.network(currentProduct.imagePath,fit: BoxFit.fitWidth,)),
//            ),
//            SizedBox(
//              height: 10.0,
//            ),
//            Expanded(
//              flex: 4,
//              child: Column(
//                children: <Widget>[
//                  Expanded(
//                      flex:3,
//                      child: getStyledTextBox(currentProduct.productName)),
//                  Expanded(
//                    flex: 2,
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                      children: <Widget>[
//                        Expanded(
//                            flex : 1,
//                            child: getStyledTextBox("\$${currentProduct.price}")),
//                        Expanded(
//                            flex : 1,
//                            child: getStyledTextBox("\$${Decimal.parse("${currentProduct.price}")*Decimal.parse("${currentProduct.quantity}")}")),
//                      ],
//                    ),
//                  ),
//                  Expanded(
//                    flex: 2,
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                      children: <Widget>[
//                        Expanded(
//                            flex : 1,
//                            child: getStyledTextBox("${currentProduct.quantity}")),
//                      ],
//                    ),
//                  ),
//                  Expanded(
//                    flex: 4,
//                    child: Row(
//                      children: <Widget>[
//                        Expanded(
//                          flex : 1,
//                            child: GestureDetector(
//                                onTap:(){setQuantity(true);},
//                                child: getStyledTextBox("+"))),
//                        Expanded(
//                            flex : 1,
//                            child: GestureDetector(
//                              onTap: (){setQuantity(false);},
//                                child: getStyledTextBox("-"))),
//                      ],
//                    ),
//                  ),
//                  Expanded(
//                    flex: 3,
//                    child: Container(
//
//                    ),
//                  )
//                ],
//              ),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//
//  Widget getStyledTextBox(String text){
//    return Padding(
//      padding: const EdgeInsets.all(2.0),
//      child: Container(
//        decoration: BoxDecoration(
//          border: Border.all(
//            color: Colors.orange,
//            width: 1.0
//          ),
//        ),
//        child: Center(
//          child: Text(text,style: TextStyle(color: Colors.red,fontSize: 22.0),),
//        ),
//      ),
//    );
//
//  }
//
//  Animation<RelativeRect> getPanelAnimation(BoxConstraints constraints) {
//    final height = constraints.biggest.height;
//    final openedHeight = height - header_height;
//    final closedHeight = -header_height;
//
//    return RelativeRectTween(
//            begin: RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0),
//            end: RelativeRect.fromLTRB(0.0, openedHeight, 0.0, closedHeight))
//        .animate(
//            CurvedAnimation(parent: widget.controller, curve: Curves.linear));
//  }
//
//  bool get isPanelVisible {
//    final AnimationStatus status = widget.controller.status;
//    return status == AnimationStatus.completed ||
//        status == AnimationStatus.forward;
//  }
//
//  Widget getListView() {
//    final screenHeight = MediaQuery.of(context).size.height;
//    final screenWidth = MediaQuery.of(context).size.width;
//    final categoryList = getDummyCategoryNames();
//
//    if (categoryList.categoryNames.length == 0 ) {
//      return Scaffold(
//        body: Center(
//            child: CircularProgressIndicator(
//          backgroundColor: Colors.orange,
//          strokeWidth: 5.0,
//        )),
//      );
//    }
//    return Scaffold(
//      body: ListView.builder(
//        itemBuilder: (BuildContext context, int index) {
//          final categoryName = categoryList.categoryNames[index];
//          return Padding(
//            padding: index == categoryList.categoryNames.length-1 ? EdgeInsets.fromLTRB(3.0, 3.0, 3.0, 40.0): EdgeInsets.all(3.0) ,
//            child: Card(
//              elevation: 3.0,
//              child: Container(
//                height: screenHeight / 2,
//                child: Column(
//                  children: <Widget>[
//                    Expanded(
//                      flex: 1,
//                      child: Container(
//                        width: double.infinity,
//                        decoration: BoxDecoration(
//                          gradient: LinearGradient(
//                            begin: Alignment.topLeft,
//                            end: Alignment.bottomRight,
//                            stops: [0.2, 0.7, 0.8, 0.9],
//                            colors: [
//                              Colors.orange[800],
//                              Colors.orange[700],
//                              Colors.orange[600],
//                              Colors.orange[500],
//                            ],
//                          ),
//                        ),
//
//                        child: Center(
//                          child: Text(
//                            categoryList.categoryNames[index],
//                            style: TextStyle(
//                                fontSize: 22.0,
//                                fontWeight: FontWeight.bold,
//                                color: Colors.white,
//                                fontFamily: 'Indie'),
//                          ),
//                        ),
//                      ),
//                    ),
//                    Expanded(
//                      flex: 8,
//                      child: ListView.builder(
//                          scrollDirection: Axis.horizontal,
//                          itemCount: getCategoryProducts(categoryName).length,
//                          itemBuilder: (BuildContext context, int index) {
//                            return GestureDetector(
//                              onTap: (){flingPanel(getCategoryProducts(categoryName)[index]);},
//                              child: AspectRatio(
//                                aspectRatio: 100 / 120,
//                                child: Padding(
//                                  padding: EdgeInsets.all(4.0),
//                                  child: Card(
//                                    elevation: 1.0,
//                                    child: Column(
//                                      children: <Widget>[
//                                        Expanded(
//                                          flex: 7,
//                                          child: Container(
//                                            width: screenWidth / 3 * 2,
////                                        height: screenHeight/3,
//                                            child: Image.network(
//                                              getCategoryProducts(categoryName)[index].imagePath,
//                                              fit: BoxFit.fill,
//                                            ),
//                                          ),
//                                        ),
//                                        Expanded(
//                                          flex: 3,
//                                          child: Container(
//                                            decoration: BoxDecoration(
//                                              gradient: LinearGradient(
//                                                begin: Alignment.topLeft,
//                                                end: Alignment.bottomRight,
//                                                stops: [0.2, 0.7, 0.8, 0.9],
//                                                colors: [
//                                                  Colors.orange[800],
//                                                  Colors.orange[700],
//                                                  Colors.orange[600],
//                                                  Colors.orange[500],
//                                                ],
//                                              ),
//                                            ),
//                                            width: screenWidth / 3 * 2,
//                                            child: Padding(
//                                              padding: const EdgeInsets.only(
//                                                  left: 5.0),
//                                              child: Column(
//                                                crossAxisAlignment:
//                                                    CrossAxisAlignment.start,
//                                                mainAxisAlignment:
//                                                    MainAxisAlignment.spaceAround,
//                                                children: <Widget>[
//                                                  Text(
//                                                    getCategoryProducts(categoryName)[index].productName,
//                                                    softWrap: true,
//                                                    overflow: TextOverflow.ellipsis,
//                                                    maxLines: 2,
//                                                    style: TextStyle(
//                                                        fontSize: 18.0,
//                                                        fontWeight:
//                                                            FontWeight.bold,
//                                                        color: Colors.white,
//                                                        fontFamily: 'Indie'),
//                                                  ),
//                                                  Padding(
//                                                    padding:
//                                                        const EdgeInsets.only(
//                                                            left: 10.0),
//                                                    child: Text(
//                                                      "\$${getCategoryProducts(categoryName)[index].price}",
//                                                      style: TextStyle(
//                                                          fontSize: 16.0,
//                                                          fontWeight:
//                                                              FontWeight.bold,
//                                                          color: Colors.white,
//                                                          fontFamily: 'Charm'),
//                                                    ),
//                                                  ),
//                                                ],
//                                              ),
//                                            ),
//                                          ),
//                                        )
//                                      ],
//                                    ),
//                                  ),
//                                ),
//                              ),
//                            );
//                          }),
//                    )
//                  ],
//                ),
//              ),
//            ),
//          );
//        },
//        itemCount: categoryList.categoryNames.length,
//      ),
//    );
//  }
//
//  List<Product> getCategoryProducts(String categoryName){
//    List<Product> products = new List();
//
//    for(var product in allProducts){
//      if(product.categoryName == categoryName){
//        products.add(product);
//      }
//    }
//    return products;
//  }
//
//  void setQuantity(bool isIncrease){
//    for(var product in allProducts){
//      if(product.productName == currentProduct.productName){
//        setState(() {
//          if(isIncrease){
//            product.quantity++;
//          }else{
//            if(product.quantity == 0){
//              return;
//            }
//            product.quantity--;
//          }
//        });
//        break;
//      }
//    }
//  }
//
//}
//
//
//
//List<Product> getAllProductList() {
//  List<Product> products = new List();
//
//  products.add(new Product("Coke", getDummyCategoryNames().categoryNames[0],'https://www.thebrobasket.com/wp-content/uploads/2016/06/Rum-and-Coke-141498-gal4.jpg', 1.07));
//  products.add(new Product("Sprite", getDummyCategoryNames().categoryNames[0],'https://www.coca-cola.co.uk/content/dam/journey/gb/en/hidden/Products/lead-brand-image/sprite-lead-image-596x334-gb.jpg', 1.07));
//  products.add(new Product("Orange Juice", getDummyCategoryNames().categoryNames[0],'https://img.etimg.com/thumb/msid-62564999,width-643,imgsize-275146,resizemode-4/yes-it-is-healthy-drinking-100-per-cent-fruit-juice-does-not-raise-blood-sugar-levels.jpg', 1.87));
//  products.add(new Product("Double Burger", getDummyCategoryNames().categoryNames[1],'https://i1.wp.com/topsecretrecipes.com/foodhackerblog/wp-content/uploads/2018/03/In-n-out-double-double_top.jpg?fit=700%2C503&ssl=1', 3.07));
//  products.add(new Product("Cheese Burger", getDummyCategoryNames().categoryNames[1],'https://www.thedailymeal.com/sites/default/files/2014/09/25/cheeseburger_1.jpg', 2.07));
//  products.add(new Product("Angus Burger", getDummyCategoryNames().categoryNames[1],'https://i.pinimg.com/736x/10/4d/98/104d982199f7b048b6179a6cae4c65b4--angus-burger-mcdonalds.jpg', 4.07));
//  products.add(new Product("Chicken Breast Salad", getDummyCategoryNames().categoryNames[2],'https://skinnyms.com/wp-content/uploads/2015/11/Skillet-Chicken-Herbs-with-Garden-Salad-1-750x499.jpg', 3.57));
//  products.add(new Product("Arugula Salad", getDummyCategoryNames().categoryNames[2],'https://www.tasteslovely.com/wp-content/uploads/2014/04/Arugula-Salad-+-My-Favorite-Balsamic-Vinaigrette-tasteslovely.com_1.jpg', 3.27));
//  products.add(new Product("Fajita Chicken Salad", getDummyCategoryNames().categoryNames[2],'http://assets.kraftfoods.com/recipe_images/opendeploy/106211_640x428.jpg', 4.07));
//
//  return products;
//}
//
//Categories getDummyCategoryNames(){
//  Categories categories = new Categories();
//  categories.categoryNames.add("Beverage");
//  categories.categoryNames.add("Hamburger");
//  categories.categoryNames.add("Salad");
//
//  return categories;
//
//}
//
//class Categories {
//  List<String> categoryNames = new List();
//}
//
//class Product {
//  String productName,categoryName,imagePath;
//  double price;
//  int quantity=0;
//
//  Product(this.productName, this.categoryName,this.imagePath,this.price);
//}
