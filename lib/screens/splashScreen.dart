import 'dart:async';

import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  AnimationController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = AnimationController(
        duration: Duration(milliseconds: 4000), vsync: this);
    Timer(Duration(milliseconds: 4000),()=>Navigator.of(context).pushNamed('/mainScreen'));
  }

  @override
  Widget build(BuildContext context) {
    controller.forward();
    return Scaffold(
      body: AnimatedLogo(
        controller: controller.view,
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    super.dispose();
  }
}

class AnimatedLogo extends StatelessWidget {
  final Animation<double> controller;
  final Animation<double> transformAnimation;
  final Animation<double> sizeAnimation;

  AnimatedLogo({Key key, this.controller})
      : transformAnimation = Tween<double>(
          begin: -1.0,
          end: 0.2,
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.0,
              0.500,
              curve: Curves.elasticIn,
            ),
          ),
        ),
        sizeAnimation = Tween<double>(begin: 40.0, end: 60.0).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.500,
              1.00,
              curve: Curves.bounceIn,
            ),
          ),
        ),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: controller,
        builder: (BuildContext context, Widget child) {
          return Container(
            color: Colors.orange,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment(transformAnimation.value,0.0),
                  child: Text(
                    'Slide Menu',
                    style: TextStyle(
                        fontSize: sizeAnimation.value,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Indie',
                        color: Colors.white),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
