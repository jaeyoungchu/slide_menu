//import 'package:flutter/material.dart';
//
//Future<bool> getStyledDialog(context, name) {
//  return showDialog(
//      context: context,
//      barrierDismissible: true,
//      builder: (BuildContext context) {
//        return Dialog(
//          shape:
//              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//          child: Container(
//            height: 250.0,
//            width: 200.0,
//            decoration: BoxDecoration(
//              borderRadius: BorderRadius.circular(20.0),
//            ),
//            child: Column(
//              children: <Widget>[
//                Stack(
//                  children: <Widget>[
//                    Container(
//                      height: 50.0,
//                      decoration: BoxDecoration(
//                          borderRadius: BorderRadius.only(
//                              topRight: Radius.circular(10.0),
//                              topLeft: Radius.circular(10.0)),
//                          color: Colors.deepOrange),
//                      child: Center(
//                          child: Text(
//                        "Enter Pin Number",
//                        style: TextStyle(
//                            color: Colors.white,
//                            fontWeight: FontWeight.bold,
//                            fontSize: 24.0,
//                        ),
//                      )),
//                    ),
//                  ],
//                ),
//                SizedBox(
//                  height: 20.0,
//                ),
//                Padding(
//                  padding: EdgeInsets.all(10.0),
//                  child: Text(
//                    name,
//                    style: TextStyle(
//                      fontSize: 14.0,
//                      fontWeight: FontWeight.w300,
//                    ),
//                  ),
//                ),
//                SizedBox(
//                  height: 15.0,
//                ),
//                FlatButton(
//                  child: Center(
//                    child: Text(
//                      'Okay',
//                      style: TextStyle(fontSize: 14.0, color: Colors.teal),
//                    ),
//                  ),
//                  onPressed: () {
//                    Navigator.of(context).pop();
//                  },
//                  color: Colors.transparent,
//                ),
//              ],
//            ),
//          ),
//        );
//      });
//}
